# OpenHPC varibles

        sms_name=vhpc1    # Hostname for System Management Server
     sms_ip=192.168.100.1 # Internal IP address on SMS server
sms_eth_internal=ens10    # Internal Ethernet interface on SMS
   eth_provision=ens10    # Provisioning interface for computes
internal_netmask=255.255.255.0  # Subnet netmask for internal network
ntp_server=${sms_ip}            # Local ntp server for time synchronization
bmc_username="root"		# BMC username for use by IPMI
bmc_password="abc123"		# BMC password for use by IPMI
num_computes=2           # Total # of desired compute nodes

# Host names for computes
c_name=(
    vhpc2
    vhpc3
)

# compute node ip addresses
c_ip=(
    192.168.100.2
    192.168.100.3
)

# MAC addresses for computes
# these mac addresses are just examples, use the ones associate with the
# virtual machines that you create 
c_mac=(
   52:54:00:c7:9d:b8
   52:54:00:c7:9d:b9
)

# BMC addresses for compute nodes
# these mac addresses are just examples, use the ones associate with the
# virtual machines that you create
#c_bmc=(
#   52:54:00:c7:9d:ba
#   52:54:00:c7:9d:bb
#)

compute_regex="vhpc*"  # Regex matching all compute node names (e.g. “c*”)
compute_prefix="vhpc"  # Prefix for compute node names (e.g. “c”)


# Optional
#${sysmgmtd_host}	# BeeGFS System Management host name
#${mgs_fs_name}	# Lustre MGS mount name
#${sms_ipoib}		# IPoIB address for SMS server
#${ipoib_netmask}	# Subnet netmask for internal IPoIB
#${c_ipoib[0]}, ${c_ipoib[1]}, ...	# IPoIB addresses for computes
${kargs}				# Kernel boot arguments
${nagios_web_password}		# Nagios web access password

export CHROOT=/opt/ohpc/admin/images/rocky8.7

# if using an alternative package repository, set here
export YUM_MIRROR=

# 3.9.1
WW_CONF=/etc/warewulf/bootstrap.conf

# Report

printf '%s: %s\n' "Hostname for SMS server"          "${sms_name}"
printf '%s: %s\n' "Internal IP address on SMS server" "${sms_ip}"
printf '%s: %s\n' "Internal Ethernet interface on SMS" "${sms_eth_internal}"
printf '%s: %s\n' "Provisioning interface for computes" "${eth_provision}"
printf '%s: %s\n' "Subnet netmask for internal network"      "${internal_netmask}"
printf '%s: %s\n' "Local ntp server for time synchronization" "${ntp_server}"

printf '%s: %s\n' "BMC username for use by IPMI" "${bmc_username}"
printf '%s: %s\n' "BMC password for use by IPMI" "${bmc_password}"

printf '%s: %s\n' "Total # of desired compute nodes" "${num_computes}"


printf "compute node info\n"
printf "name: ip, mac\n"
for (( j=0; j<${num_computes}; j++ ));
do
   printf "%s: %s, %s\n" "${c_name[$j]}"  "${c_ip[$j]}" "${c_mac[$j]}"
done

#${sms_name}             # Hostname for SMS server
#${sms_ip}               # Internal IP address on SMS server
#${sms_eth_internal}     # Internal Ethernet interface on SMS
#${eth_provision}        # Provisioning interface for computes
#${internal_netmask}     # Subnet netmask for internal network
#${ntp_server}           # Local ntp server for time synchronization
#${bmc_username}         # BMC username for use by IPMI
#${bmc_password}         # BMC password for use by IPMI
#${num_computes}         # Total # of desired compute nodes

#${c_ip[0]}, ${c_ip[1]}, ...     # Desired compute node addresses
#${c_bmc[0]}, ${c_bmc[1]}, ...   # BMC addresses for computes
#${c_mac[0]}, ${c_mac[1]}, ...   # MAC addresses for computes
#${c_name[0]}, ${c_name[1]}, ... # Host names for computes
#${compute_regex}                # Regex matching all compute node names (e.g. “c*”)
#${compute_prefix}               # Prefix for compute node names (e.g. “c”)

# Optional
#${sysmgmtd_host}        # BeeGFS System Management host name
#${mgs_fs_name}  # Lustre MGS mount name
#${sms_ipoib}            # IPoIB address for SMS server
#${ipoib_netmask}        # Subnet netmask for internal IPoIB
#${c_ipoib[0]}, ${c_ipoib[1]}, ...       # IPoIB addresses for computes
#${kargs}                                # Kernel boot arguments
#${nagios_web_password}          # Nagios web access password

